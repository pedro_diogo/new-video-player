import { Component, OnInit } from '@angular/core';
import { Video } from '../models/video.model';

import { BookmarkService } from '../services/bookmark.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent implements OnInit {

  bookmarks: Video[];

  constructor(private bookmarkService: BookmarkService) { }

  ngOnInit(): void {
    this.getBookmarks();
  }

  getBookmarks(): void{
    this.bookmarkService.getBookmarks().subscribe(bookmarks => {
      this.bookmarks = bookmarks;
    });
  }

  delete(bookmark: Video): void {
    this.bookmarks = this.bookmarks.filter(b => b !== bookmark);
    this.bookmarkService.deleteBookmark(bookmark).subscribe();
  }

}
