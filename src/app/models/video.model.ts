import { SafeResourceUrl } from '@angular/platform-browser';

export interface Video {
    id: number;
    url: string;
    safeUrl: SafeResourceUrl;
}
