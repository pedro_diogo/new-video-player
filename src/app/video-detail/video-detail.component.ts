import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../models/video.model';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { VideoService } from '../services/video.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.css']
})
export class VideoDetailComponent implements OnInit {

  safeUrl: any;
  @Input() video: Video;


  constructor(
    private route: ActivatedRoute,
    private videoService: VideoService,
    private location: Location,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.getVideo();
  }

  getVideo(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.videoService.getVideo(id)
      .subscribe(video => {
        this.video = video;
        const videoId = video.url.substr(video.url.length - 11);
        const embedUrl = 'https://www.youtube.com/embed/' + videoId;
        const safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(embedUrl);

        this.video.safeUrl = safeUrl;

      } );
  }

}
