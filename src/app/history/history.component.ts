import { Component, OnInit } from '@angular/core';
import { Video } from '../models/video.model';

import { VideoService } from '../services/video.service';
import { BookmarkService } from '../services/bookmark.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  selectedVideo: Video;


  videos: Video[];
  error: any;

  constructor(private videoService: VideoService, private bookmarkService: BookmarkService) { }

  ngOnInit(): void {
    this.getVideos();
  }

  // getVideos(): void {
  //   this.videoService.getVideos().subscribe(videos => this.videos = videos);
  // }

  getVideos(): void{
    this.videoService.getHistoryItems().subscribe(
      (videos: Video[]) => { this.videos = videos; },
      (error: any) => { this.error = error; console.log('erro: ', this.error); }
    );
  }

  add(url: string): void {
    url = url.trim();
    if (!url) { return; }

    this.videoService.addVideo({ url } as Video)
      .subscribe(video => {
        this.videos.push(video);
        console.log('recebi isso de volta', video);
        // this.onSelect(video);
      });
  }

  delete(video: Video): void {
    this.videos = this.videos.filter(v => v !== video);
    this.videoService.deleteVideo(video).subscribe();
  }

  addBookmark(video: Video): void {
    this.bookmarkService.addBookmark(video).subscribe();
  }

}
