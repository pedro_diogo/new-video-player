// import { Injectable } from '@angular/core';
// import { InMemoryDbService } from 'angular-in-memory-web-api';
// import { Video } from '../models/video.model';
// import { DomSanitizer } from '@angular/platform-browser';


// @Injectable({
//   providedIn: 'root',
// })
// export class InMemoryDataService implements InMemoryDbService {

//   constructor(private sanitizer: DomSanitizer){}
//   // tslint:disable-next-line: typedef
//   createDb() {
//     const videos = [
//       { id: 11, url: 'https://www.youtube.com/embed/kJQP7kiw5Fk', safeUrl: {} },
//       { id: 12, url: 'https://www.youtube.com/watch?v=XqZsoesa55w', safeUrl: {} },
//       { id: 13, url: 'https://www.youtube.com/watch?v=JGwWNGJdvx8', safeUrl: {} },
//       { id: 14, url: 'https://www.youtube.com/watch?v=RgKAFK5djSk', safeUrl: {} }
//     ];

//     const bookmarks = [
//       { id: 13, url: 'https://www.youtube.com/watch?v=JGwWNGJdvx8', safeUrl: {} },
//       { id: 14, url: 'https://www.youtube.com/watch?v=RgKAFK5djSk', safeUrl: {} }
//     ];

//     return {videos};
//   }

//   // Overrides the genId method to ensure that a hero always has an id.
//   // If the heroes array is empty,
//   // the method below returns the initial number (11).
//   // if the heroes array is not empty, the method below returns the highest
//   // hero id + 1.
//   genId(videos: Video[]): number {
//     return videos.length > 0 ? Math.max(...videos.map(video => video.id)) + 1 : 11;
//   }
// }
