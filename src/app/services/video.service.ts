import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { SafeResourceUrl } from '@angular/platform-browser';
import { DomSanitizer } from '@angular/platform-browser';

import { Video } from '../models/video.model';

@Injectable({
  providedIn: 'root'
})

export class VideoService {

  videoId: string;
  embedUrl: string;
  safeUrl: SafeResourceUrl;

  // URLs to web api
  // private videosUrl = 'api/videos';
  private apiRoot = 'http://localhost:8000/history-entry';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  // tslint:disable-next-line: typedef
  getHistoryItems() {
    return this.http.get<Video[]>(this.apiRoot).pipe(
      catchError(this.handleError<Video[]>('getVideos', []))
    );
  }


  constructor(private http: HttpClient, private sanitizer: DomSanitizer ) { }

  /** GET videos from the server */
  // getVideos(): Observable<Video[]> {
  //   return this.http.get<Video[]>(this.videosUrl).pipe(
  //     catchError(this.handleError<Video[]>('getVideos', []))
  //   );
  // }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // tslint:disable-next-line: typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** POST: add a new video to the server */
  addVideo(video: Video): Observable<Video> {
    return this.http.post<Video>(this.apiRoot.concat('/'), video, this.httpOptions).pipe(
      catchError(this.handleError<Video>('addVideo'))
    );
  }

  getVideo(id: number): Observable<Video> {
    const url = `${this.apiRoot}/${id}`;
    return this.http.get<Video>(url).pipe(
      catchError(this.handleError<Video>(`getVideo id=${id}`))
    );
  }

  /** DELETE: delete the hero from the server */
  deleteVideo(video: Video | number): Observable<Video> {
    const id = typeof video === 'number' ? video : video.id;
    const url = `${this.apiRoot}/${id}`;

    return this.http.delete<Video>(url, this.httpOptions).pipe(
      catchError(this.handleError<Video>('deleteVideo'))
    );
  }
}
