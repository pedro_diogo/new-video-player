import { Injectable } from '@angular/core';
import { VIDEOS } from '../mock-videos';
import { Video } from '../models/video.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

  bookmarks: Video[] = [];

  // bookmarkSubject: BehaviorSubject<Video[]>;
  bookmarkSubject = new BehaviorSubject(this.bookmarks);

  constructor() {}

  getBookmarks(): Observable<Video[]> {
    return this.bookmarkSubject.asObservable();
  }

  deleteBookmark(bookmark: Video): Observable<Video[]> {
    this.bookmarks = this.bookmarks.filter(b => b !== bookmark);
    this.bookmarkSubject.next(this.bookmarks);
    return this.bookmarkSubject.asObservable();
  }

  addBookmark(bookmark: Video): Observable<Video[]> {
    if ( this.bookmarks.length === 0 // bookmark list is empty
      || !this.bookmarks.some(video => video.url === bookmark.url)) { // there is no duplicate in the list

      this.bookmarks.push(bookmark);
    }
    this.bookmarkSubject.next(this.bookmarks);
    return this.bookmarkSubject.asObservable();
  }
}
