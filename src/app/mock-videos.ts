import { Video } from './models/video.model';

export const VIDEOS: Video[] = [
  { id: 14, url: 'https://www.youtube.com/watch?v=RgKAFK5djSk', safeUrl: {} },
  { id: 15, url: 'https://www.youtube.com/watch?v=zFzwgFO2bsg', safeUrl: {} },
  { id: 16, url: 'https://www.youtube.com/watch?v=zFzwgFO2bsg', safeUrl: {} },
  { id: 17, url: 'https://www.youtube.com/watch?v=zFzwgFO2bsg', safeUrl: {} },
  { id: 18, url: 'https://www.youtube.com/watch?v=zFzwgFO2bsg', safeUrl: {} }
];
